#include <iostream>
#include <unordered_set>
using namespace std;

class Node
{
public:
    int data;
    Node *next;

    Node(int val)
    {
        data = val;
        next = nullptr;
    }
};
void removeDuplicates(Node *&head)
{
    if (head == nullptr)
    {
        return;
    }

    unordered_set<int> seen;
    Node *current = head;
    Node *prev = nullptr;

    while (current != nullptr)
    {
        if (seen.find(current->data) != seen.end())
        {

            prev->next = current->next;
            delete current;
            current = prev->next;
        }
        else
        {
            seen.insert(current->data);
            prev = current;
            current = current->next;
        }
    }
}
void printList(Node *head)
{
    while (head != nullptr)
    {
        cout << head->data << " ";
        head = head->next;
    }
    cout << endl;
}

int main()
{
    Node *head = new Node(1);
    head->next = new Node(2);
    head->next->next = new Node(3);
    head->next->next->next = new Node(2);
    head->next->next->next->next = new Node(4);
    head->next->next->next->next->next = new Node(1);
    head->next->next->next->next->next->next = new Node(5);
    cout << "Original Linked List:" << endl;
    printList(head);

    removeDuplicates(head);

    cout << "Linked List after Removing Duplicates:" << endl;
    printList(head);

    return 0;
}
