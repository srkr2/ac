import heapq
from collections import defaultdict

def build_huffman_tree(frequencies):
    heap = []
    for char, weight in frequencies.items():
        heap.append([weight, [char, ""]])    
    heapq.heapify(heap)

    while len(heap) > 1:
        
        lo = heapq.heappop(heap)
        hi = heapq.heappop(heap)

        
        for pair in lo[1:]:
            pair[1] = '0' + pair[1]
        for pair in hi[1:]:
            pair[1] = '1' + pair[1]

        
        heapq.heappush(heap, [lo[0] + hi[0]] + lo[1:] + hi[1:])

   
    return heap[0][1:]

def huffman_coding(text):
    
    frequencies = defaultdict(int)
    for char in text:
        frequencies[char] += 1

   
    huffman_tree = build_huffman_tree(frequencies)

    for char, code in huffman_tree:
        print(f"{char}: {code}")



text = input("Enter a string: ")


huffman_coding(text)
