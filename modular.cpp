#include<iostream>
using namespace std;
class Node{
    public:
    int data;
    Node *next;
    Node(int val){
        data = val;
        next = nullptr;
    }
};
void printModular(Node *head,int k){
    if(k<0 || head == nullptr){
        return;
    }
    int i=1;
    Node *curr = head, *mod = nullptr;
    while(curr != nullptr){
        if(i%k == 0){
            mod = curr;
        }
        curr = curr->next;
        i++;
    }
    cout<<"Modular node is: "<<mod->data;
}
int main(){
    Node* head = new Node(3);
    head->next = new Node(7);
    head->next->next = new Node(1);
    head->next->next->next = new Node(9);
    head->next->next->next->next = new Node(8);
    printModular(head,2);
    return 0;
}
