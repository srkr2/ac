#include<iostream>
using namespace std;

int main()
{
    int n= 7;
    for (int i = 0; i < n - 1; i++) {
        for (int space = 0; space < n - i -1; space++) {
            cout << " ";
        }
        for (int j = 0; j < 2 * i + 1; j++) {
            if (j == 0 || j == 2 * i)
                cout << "*";
            else
                cout << " ";
        }
        cout << endl;
    }
    for (int i = 0; i < 2 * n - 1; i++) {
        cout << "*";
    }
    return 0;
}