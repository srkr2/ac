#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int main() {
    vector<int> numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int target;
    cout << "Enter the element to search: ";
    cin >> target;

    auto result = find(numbers.begin(), numbers.end(), target);
    if (result != numbers.end()) {
        cout << "Element found at index: " << distance(numbers.begin(), result) << endl;
    } else {
        cout << "Element not found." << endl;
    }
    return 0;
}
