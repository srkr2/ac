#include <bits/stdc++.h>
using namespace std;

int maxSubsequenceSubstring(string &X, string &Y, int n, int m)
{
    if (n == 0 || m == 0)
        return 0;
    if (X[n - 1] == Y[m - 1])
    {
        return 1 + maxSubsequenceSubstring(X, Y, n - 1, m - 1);
    }
    else
    {
        return maxSubsequenceSubstring(X, Y, n - 1, m);
    }
}

int main()
{
    string X = "abcda";
    string Y = "bcdadd";
    int n = max(X.size(),Y.size());
    int maximum_length = 0;
    for (int i = 0; i <= n; i++)
    {
        int temp_ans = maxSubsequenceSubstring(X, Y, n, i);
        maximum_length = max(maximum_length,temp_ans);
    }
    cout <<maximum_length;
    return 0;
}