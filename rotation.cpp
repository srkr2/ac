#include <iostream>
using namespace std;

void printMatrix(int mat[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << mat[i][j] << "\t";
        }
        cout << endl;
    }
}

void rotateClockwise270(int a[3][3], int b[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 2, k = 0; j >= 0; j--, k++) {
            b[i][k] = a[i][j];
        }
    }
}

void rotateAntiClockwise90(int a[3][3], int b[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0, k = 2; k>=0; j++, k--) {
            b[i][k] = a[i][j];
        }
    }
}

int main() {
    int a[3][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    int b[3][3];

    cout << "Original Matrix:" << endl;
    printMatrix(a);

    rotateClockwise270(a, b);
    cout << "\nMatrix after rotating clockwise by 270 degrees:" << endl;
    printMatrix(b);

    rotateAntiClockwise90(a, b);
    cout << "\nMatrix after rotating anticlockwise by 90 degrees:" << endl;
    printMatrix(b);

    return 0;
}
