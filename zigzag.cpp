#include <iostream>
using namespace std;

void printZigZag(int matrix[3][3])
{
    int m = 3, n = 3;
    int i = 0, j = 0, k = 1;
    cout << matrix[0][0] << " ";
    while (k < m * n)
    {
        while (i >= 1 && j < n - 1)
        {
            i--;
            j++;
            k++;
            cout << matrix[i][j] << " ";
        }
        if (j < n - 1)
        {
            j++;
            k++;
            cout << matrix[i][j] << " ";
        }
        else if (i < m - 1)
        {
            i++;
            k++;
            cout << matrix[i][j] << " ";
        }
        while (j >= 1 && i < m - 1)
        {
            j--;
            i++;
            k++;
            cout << matrix[i][j] << " ";
        }
        if (i < m - 1)
        {
            i++;
            k++;
            cout << matrix[i][j] << " ";
        }
        else if (j < n - 1)
        {
            j++;
            k++;
            cout << matrix[i][j] << " ";
        }
    }
}

int main()
{
    int givenMatrix[3][3] = {{1, 2, 3},
                             {4, 5, 6},
                             {7, 8, 9}};

    cout << "Matrix in Zig-Zag:" << endl;
    printZigZag(givenMatrix);

    return 0;
}
