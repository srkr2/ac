#include <iostream>
using namespace std;
void fillSpiralClockwise(char matrix[3][3]) {
    char currentChar = 'A';
    int rows = 3, cols = 3;
    int top = 0, bottom = rows - 1, left = 0, right = cols - 1;

    while (top <= bottom && left <= right) {
        for (int i = left; i <= right; i++) {
            matrix[top][i] = currentChar++;
        }
        top++;

        for (int i = top; i <= bottom; i++) {
            matrix[i][right] = currentChar++;
        }
        right--;
        for (int i = right; i >= left; i--) {
            matrix[bottom][i] = currentChar++;
        }
        bottom--;

        for (int i = bottom; i >= top; i--) {
            matrix[i][left] = currentChar++;
        }
        left++;
    }
}

void printMatrix(char matrix[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    char spiralMatrix[3][3];

    fillSpiralClockwise(spiralMatrix);
    cout << "Spiral Clockwise Matrix:" << endl;
    printMatrix(spiralMatrix);

    return 0;
}
