#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
void printPermutations(vector<int>& elements) {
    do {
        for (int element : elements) {
            cout << element << " ";
        }
        cout << endl;
    } while (next_permutation(elements.begin(), elements.end()));
}

int main() {
    int n;

   
    cout << "Enter the number of elements: ";
    cin >> n;

    
    vector<int> elements(n);
    cout << "Enter the elements: ";
    for (int i = 0; i < n; ++i) {
        cin >> elements[i];
    }

    
    sort(elements.begin(), elements.end());

   
    cout << "Permutations:\n";
    printPermutations(elements);

    return 0;
}
