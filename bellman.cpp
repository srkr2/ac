#include <iostream>
#include <vector>
#include <climits>
using namespace std;

struct Edge {
    int source, destination, weight;
};

void bellmanFord( vector<Edge>& edges, int vertices, int source) {
    vector<int> distance(vertices, INT_MAX);
    distance[source] = 0;

    for (int i = 1; i <= vertices - 1; ++i) {
        for ( auto& edge : edges) {
            if (distance[edge.source] != INT_MAX && distance[edge.source] + edge.weight < distance[edge.destination]) {
                distance[edge.destination] = distance[edge.source] + edge.weight;
            }
        }
    }

    for (auto& edge : edges) {
        if (distance[edge.source] != INT_MAX && distance[edge.source] + edge.weight < distance[edge.destination]) {
            cout << "Graph contains a negative weight cycle!\n";
            return;
        }
    }

    cout << "Shortest distances from source " << source << ":\n";
    for (int i = 0; i < vertices; ++i) {
        cout << "Vertex " << i << ": " << distance[i] << '\n';
    }
}

int main() {

    vector<Edge> edges = {
        {0, 1, 6},
        {0, 2, 5},
        {0, 3, 5},
        {1, 4, -1},
        {2, 1, -2},
        {2, 4, 1},
        {3, 2, -2},
        {3, 5, -1},
        {4, 6, 3},
        {5, 6, 3}
    };

    int vertices = 7;
    int source = 0;

    bellmanFord(edges, vertices, source);

    return 0;
}
