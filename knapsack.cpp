#include <iostream> 
using namespace std; 
  

int knapSack(int W, int wt[], int profit[], int n) 
{ 
    if (n == 0 || W == 0) 
        return 0; 

    if (wt[n - 1] > W) 
        return knapSack(W, wt, profit, n - 1); 
  
    else
        return max( 
            profit[n - 1] 
                + knapSack(W - wt[n - 1], wt, profit, n - 1), 
            knapSack(W, wt, profit, n - 1)); 
} 

int main() 
{ 
    int profit[] = { 60, 100, 120 }; 
    int weight[] = { 10, 20, 30 }; 
    int W = 50; 
    int n = 3; 
    cout << knapSack(W, weight, profit, n); 
    return 0; 
} 