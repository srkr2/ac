#include <iostream>
#include <stack>
using namespace std;
int main()
{
    stack<int> myStack;

    myStack.push(1);
    myStack.push(2);
    myStack.push(3);
    cout << "Top element of the stack: " << myStack.top() << endl;

    myStack.pop();

    cout<<"Pop\nNew top element is: "<<myStack.top()<<endl;
    if (myStack.empty())
    {
        cout << "Stack is empty." << endl;
    }
    else
    {
        cout << "Stack is not empty." << endl;
    }

    return 0;
}