#include <iostream>
using namespace std;
class TreeNode {
public:
    int value;
    TreeNode* left;
    TreeNode* right;

    TreeNode(int val){
        value = val;
        left = nullptr;
        right = nullptr;
    }
};

void flattenBSTToLinkedList(TreeNode* root, TreeNode*& prev, TreeNode*& head) {
    if (!root) {
        return;
    }

    flattenBSTToLinkedList(root->left, prev, head);

    if (prev) {
        root->left = nullptr;
        prev->right = root;
    } else {
        head = root;
    }

    prev = root;

    flattenBSTToLinkedList(root->right, prev, head);
}

void printLinkedList(TreeNode* head) {
    while (head) {
        cout << head->value << " ";
        head = head->right;
    }
    cout << endl;
}

int main() {
    TreeNode* root = new TreeNode(4);
    root->left = new TreeNode(2);
    root->right = new TreeNode(6);
    root->left->left = new TreeNode(1);
    root->left->right = new TreeNode(3);
    root->right->left = new TreeNode(5);
    root->right->right = new TreeNode(7);

    TreeNode* head = nullptr;
    TreeNode* prev = nullptr;

    flattenBSTToLinkedList(root, prev, head);

    cout << "Linked List:" << endl;
    printLinkedList(head);

    return 0;
}
